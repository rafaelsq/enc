package main

import (
	"archive/tar"
	"fmt"
	"io"
	"os"
)

func archive(in string, w io.Writer) error {
	tw := tar.NewWriter(w)
	defer tw.Close()

	return hDir(in, tw)
}

func hFile(_path string, tw *tar.Writer, fi os.FileInfo) error {
	fr, err := os.Open(_path)
	if err != nil {
		return err
	}
	defer fr.Close()

	h := new(tar.Header)
	h.Name = _path
	h.Size = fi.Size()
	h.Mode = int64(fi.Mode())
	h.ModTime = fi.ModTime()

	err = tw.WriteHeader(h)
	if err != nil {
		return err
	}

	_, err = io.Copy(tw, fr)
	return err
}

func hDir(dirPath string, tw *tar.Writer) error {
	dir, err := os.Open(dirPath)
	if err != nil {
		return err
	}
	defer dir.Close()

	fis, err := dir.Readdir(0)
	if err != nil {
		return err
	}

	for _, fi := range fis {
		curPath := dirPath + "/" + fi.Name()
		if fi.IsDir() {
			if err := hDir(curPath, tw); err != nil {
				return err
			}
		} else {
			fmt.Printf("adding... %s\n", curPath)
			if err := hFile(curPath, tw, fi); err != nil {
				return err
			}
		}
	}

	return nil
}
