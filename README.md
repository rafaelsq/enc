# Enc

encrypt files in current folder using AES-GCM

```
$ go get -u gitlab.com/rafaelsq/enc

$ mkdir test
$ cd test
$ echo "a" > a
$ echo "b" > b
$ echo "c" > c
$ echo "d" > d

$ ls
a b c d

$ enc -e -r ./ ./
password:
Encrypt ./ to ./
enc a to: 0cc175b9c0f1b6a831c399e269772661
enc b to: 92eb5ffee6ae2fec3ad71c777531578f
enc c to: 4a8a08f09d37b73795649038408b5f33
enc d to: 8277e0910d750195b448797616e091ad
enc index to: 6a992d5529f459a44fee58c733255e86

$ ls
0cc175b9c0f1b6a831c399e269772661  6a992d5529f459a44fee58c733255e86  92eb5ffee6ae2fec3ad71c777531578f
4a8a08f09d37b73795649038408b5f33  8277e0910d750195b448797616e091ad

$ enc -d -r ./ ./
password:
Decrypt ./ to ./
dec 0cc175b9c0f1b6a831c399e269772661 to: a
dec 4a8a08f09d37b73795649038408b5f33 to: c
dec 8277e0910d750195b448797616e091ad to: d
dec 92eb5ffee6ae2fec3ad71c777531578f to: b

$ ls
a b c d

$ enc -e -t -r ./ out
password:
Encrypt ./ to out
adding... .//a
adding... .//d
adding... .//c
adding... .//b
adding... .//out

$ ls
out
```
