package main

import (
	"archive/tar"
	"bufio"
	"bytes"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"github.com/urfave/cli"
	"gitlab.com/rafaelsq/encstream"
)

var (
	INDEX_NAME string = fmt.Sprintf("%x", md5.Sum([]byte("index")))
)

func encodeFile(key, salt, iv []byte, inName, outName string) error {
	f, err := os.Open(inName)
	if err != nil {
		return fmt.Errorf("could not open file %s; %v", inName, err)
	}
	defer f.Close()

	o, err := os.Create(outName)
	if err != nil {
		return fmt.Errorf("could not open file %s; %v", outName, err)
	}
	defer o.Close()

	return encstream.Encode(key, salt, iv, f, o)
}

func decFile(pass []byte, inName, outName string) error {
	f, err := os.Open(inName)
	if err != nil {
		return err
	}
	defer f.Close()

	if outName == "-" {
		return encstream.Decode(pass, f, os.Stdout)
	} else {
		o, err := os.Create(outName)
		if err != nil {
			return err
		}
		defer o.Close()

		return encstream.Decode(pass, f, o)
	}
}

func exists(file string) (bool, error) {
	_, err := os.Stat(file)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		} else {
			return false, err
		}
	}
	return true, nil
}

func encryptAll(pass []byte, remove bool, index map[string]string, inDir, outDir string) error {
	index_path := filepath.Join(outDir, INDEX_NAME)
	if exist, err := exists(index_path); err != nil {
		return err
	} else if exist {
		fmt.Println("you should decrypt all first!")
		return nil
	}

	salt := encstream.NewSalt()
	iv := encstream.NewIV()
	key := encstream.DeriveKey(pass, salt)

	for outName, inName := range index {
		fmt.Println("enc", inName, "to:", outName)
		if err := encodeFile(key, salt, iv, filepath.Join(inDir, inName), filepath.Join(outDir, outName)); err != nil {
			return err
		}
	}

	indexFile, err := os.Create(index_path)
	if err != nil {
		panic(err)
	}
	defer indexFile.Close()

	b, err := json.Marshal(index)
	if err != nil {
		return err
	}

	fmt.Println("enc index to:", INDEX_NAME)
	err = encstream.Encode(key, salt, iv, bytes.NewReader(b), indexFile)
	if err != nil {
		return err
	}

	paths := make([]string, 0)
	if remove {
		for _, inName := range index {
			path, _ := filepath.Split(inName)
			if len(path) != 0 {
				paths = append(paths, path)
			}
			if err := os.Remove(filepath.Join(inDir, inName)); err != nil {
				fmt.Println(" remove err", err, inName)
			}
		}

		// rm -rf
		for _, dir := range paths {
			os.RemoveAll(filepath.Join(inDir, dir))
		}

		// only works if dir is empty
		os.Remove(inDir)
	}

	return nil
}

func decryptAll(pass []byte, inDir, outDir string) error {
	index_path := filepath.Join(inDir, INDEX_NAME)

	if exist, err := exists(index_path); err != nil {
		return err
	} else if !exist {
		fmt.Println("no encrypted data found in", inDir)
		return nil
	}

	index := map[string]string{}
	if file, err := os.Open(index_path); err != nil {
		return err
	} else {
		defer file.Close()

		buf := bytes.NewBuffer([]byte{})

		err = encstream.Decode(pass, file, buf)
		if err != nil {
			return err
		}

		dec := json.NewDecoder(bytes.NewReader(buf.Bytes()))
		err = dec.Decode(&index)
		if err != nil {
			return err
		}
	}

	rollback := make([]string, 0)
	shouldRollback := false
	for inName, outName := range index {
		inName = filepath.Join(inDir, inName)
		outName = filepath.Join(outDir, outName)

		if exist, err := exists(outName); err != nil {
			return err
		} else if exist {
			fmt.Println("dec", inName, "to:", outName, "file already exists")
			shouldRollback = true
			break
		}

		fmt.Println("dec", inName, "to:", outName)

		rollback = append(rollback, outName)

		dir, _ := filepath.Split(outName)
		if len(dir) != 0 {
			os.MkdirAll(dir, os.ModePerm)
		}

		if err := decFile(pass, inName, outName); err != nil {
			return err
		}
	}

	if shouldRollback {
		fmt.Println("Rollback")
		for _, fileName := range rollback {
			if exist, _ := exists(fileName); exist {
				fmt.Println(" -", fileName)
				os.Remove(fileName)
			}
		}

		return nil
	}

	return nil
}

func echo(start bool) error {
	r := "echo"
	if !start {
		r = "-echo"
	}

	cmd := exec.Command("stty", r)
	cmd.Stdin = os.Stdin
	err := cmd.Run()
	if err != nil {
		return err
	}

	return cmd.Wait()
}

func askPassword() ([]byte, error) {
	fmt.Print("password:")
	echo(false)
	line, _, err := bufio.NewReader(os.Stdin).ReadLine()
	if err != nil {
		return nil, fmt.Errorf("input err; %v", err)
	}
	echo(true)
	fmt.Print("\n")
	return line, nil
}

func enc(files []Header, out string, pass []byte, join bool) error {
	salt := encstream.NewSalt()
	iv := encstream.NewIV()
	key := encstream.DeriveKey(pass, salt)

	if join {
		fmt.Printf("creating tar \"%s\"\n", out)
		o, err := os.Create(out)
		if err != nil {
			return fmt.Errorf("could not open file %s; %v", out, err)
		}
		defer o.Close()

		var w bytes.Buffer

		tw := tar.NewWriter(&w)
		for _, h := range files {
			fmt.Printf(" adding \"%v\"\n", h.Dest)
			fr, err := os.Open(h.Src)
			if err != nil {
				return err
			}

			if err := tw.WriteHeader(&tar.Header{
				Name:    h.Dest,
				Size:    h.Size,
				Mode:    h.Mode,
				ModTime: h.ModTime,
			}); err != nil {
				return err
			}

			if _, err := io.Copy(tw, fr); err != nil {
				return err
			}

			if err := fr.Close(); err != nil {
				return err
			}
		}
		if err := tw.Close(); err != nil {
			return err
		}

		return encstream.Encode(key, salt, iv, &w, o)
	}

	// each file will be saved to a tar file
	var superErr error
	var rollback []string
	for _, h := range files {
		fmt.Printf(" ~ \"%v\" to \"%s\"\n", h.Src, h.Dest)

		dir, _ := filepath.Split(h.Dest)
		if len(dir) != 0 {
			os.MkdirAll(dir, os.ModePerm)
		}

		o, err := os.Create(h.Dest)
		if err != nil {
			superErr = err
			break
		}
		defer o.Close()

		var rw bytes.Buffer

		tw := tar.NewWriter(&rw)
		defer tw.Close()

		if err := tw.WriteHeader(&tar.Header{
			Name:    h.Src,
			Size:    h.Size,
			Mode:    h.Mode,
			ModTime: h.ModTime,
		}); err != nil {
			superErr = err
			break
		}

		fr, err := os.Open(h.Src)
		if err != nil {
			superErr = err
			break
		}
		defer fr.Close()

		if _, err := io.Copy(tw, fr); err != nil {
			superErr = err
			break
		}

		if err := encstream.Encode(key, salt, iv, &rw, o); err != nil {
			superErr = err
			break
		}

		rollback = append(rollback, h.Dest)
	}

	if superErr != nil && len(rollback) > 0 {
		fmt.Println(" rollback", superErr)
		for _, src := range rollback {
			fmt.Println("  -", src)
			if err := os.Remove(src); err != nil {
				fmt.Printf("could not remove file \"%s\"; %v\n", src, err)
			}
		}
	}

	return superErr
}

func decOld(files []Header, in, out string, pass []byte) error {
	if len(files) == 1 {
		if err := decFile(pass, files[0].Src, out); err != nil {
			return err
		}

		return nil
	}

	return decryptAll(pass, in, out)
}

func dec(files []Header, in, out string, pass []byte) error {
	if exist, _ := exists(filepath.Join(in, INDEX_NAME)); exist {
		return decOld(files, in, out, pass)
	}

	var superErr error
	var rollback []string
	for _, file := range files {
		r, w := io.Pipe()

		f, err := os.Open(file.Src)
		if err != nil {
			superErr = err
			break
		}

		go func() {
			defer f.Close()
			defer w.Close()
			if err := encstream.Decode(pass, f, w); err != nil {
				panic(err)
			}
		}()

		tr := tar.NewReader(r)
		for {
			h, err := tr.Next()

			if err == io.EOF {
				break
			}

			if err == tar.ErrHeader {
				superErr = err
				break
			}

			if err != nil {
				panic(err)
			}

			path := filepath.Join(out, h.Name)
			fi := h.FileInfo()
			if !fi.IsDir() {
				dir, _ := filepath.Split(path)
				if len(dir) != 0 {
					if err := os.MkdirAll(dir, os.ModePerm); err != nil {
						superErr = err
						break
					}
				}

				fmt.Printf("  ~ \"%s\"\n", path)
				f, err := os.Create(path)
				if err != nil {
					superErr = err
					break
				}
				if _, err := io.Copy(f, tr); err != nil {
					superErr = err
					break
				}
				rollback = append(rollback, path)
			}
		}

		if superErr == tar.ErrHeader {
			return decOld(files, in, out, pass)
		}
	}

	if superErr != nil && len(rollback) > 0 {
		fmt.Println(" rollback")
		for _, path := range rollback {
			fmt.Println("  -", path)
			if err := os.Remove(path); err != nil {
				fmt.Printf("could not remove file \"%s\"; %v\n", path, err)
			}
		}
	}

	return superErr
}

type Header struct {
	Name    string // Name of file entry
	Src     string
	Dest    string
	Size    int64     // Logical file size in bytes
	Mode    int64     // Permission and mode bits
	ModTime time.Time // Modification time
}

func main() {
	app := cli.NewApp()
	app.Name = "Enc"
	app.Usage = "encrypt or decrypt files"
	app.Version = "0.2.0"
	app.Flags = []cli.Flag{
		cli.BoolFlag{Name: "d", Usage: "Decrypt"},
		cli.BoolFlag{Name: "r", Usage: "Remove"},
		cli.BoolFlag{Name: "e", Usage: "Encrypt"},
		cli.BoolFlag{Name: "t", Usage: "Tar files"},
	}
	app.Action = func(c *cli.Context) error {
		join := c.Bool("t")
		remove := c.Bool("r")
		in := c.Args().First()
		out := c.Args().Get(1)
		if len(in) != 0 && len(out) != 0 {
			// list input files
			if exist, err := exists(in); err != nil {
				return err
			} else if !exist {
				return fmt.Errorf("input \"%s\" not found", in)
			}

			in, err := filepath.Abs(in)
			if err != nil {
				return err
			}
			inBase := filepath.Base(in)

			if join && out != "-" {
				if exist, err := exists(out); err != nil {
					return err
				} else if exist {
					return fmt.Errorf("output \"%s\" already exists", out)
				}
			}

			files := []Header{}
			err = filepath.Walk(in, func(p string, i os.FileInfo, err error) error {
				if err != nil {
					panic(err)
				}

				if !i.IsDir() {
					var dest string
					if !join {
						if out != "-" {
							dest = filepath.Join(out, fmt.Sprintf("%x", md5.Sum([]byte(p))))
							if exist, err := exists(dest); err != nil {
								return err
							} else if exist {
								return fmt.Errorf("output \"%s\" already exists", dest)
							}
						}
					} else {
						abs, err := filepath.Abs(p)
						if err != nil {
							return err
						}

						dest = strings.TrimPrefix(abs, in)
						dest = filepath.Join("./", inBase, dest)
					}

					files = append(files, Header{
						Name:    i.Name(),
						Src:     p,
						Dest:    dest,
						Size:    i.Size(),
						Mode:    int64(i.Mode()),
						ModTime: i.ModTime(),
					})
				}

				return nil
			})
			if err != nil {
				return err
			}

			if len(files) == 0 {
				return fmt.Errorf("input \"%s\" is empty", in)
			}

			pass, err := askPassword()
			if err != nil {
				return err
			}

			if c.Bool("e") {
				fmt.Printf("Encrypt \"%s\" to \"%s\"\n", in, out)
				err = enc(files, out, pass, c.Bool("t"))
			} else if c.Bool("d") {
				fmt.Printf("Decrypt \"%s\"\n", in)
				err = dec(files, in, out, pass)
			} else {
				return fmt.Errorf("what you want to do?")
			}

			if err == nil && remove {
				fmt.Printf("removing \"%s\"\n", in)
				if err := os.RemoveAll(in); err != nil {
					fmt.Printf(" could not remove file \"%s\"; %v\n", in, err)
				}
			}
			return err
		}

		return cli.ShowAppHelp(c)
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
