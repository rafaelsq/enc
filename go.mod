module github.com/rafaelsq/enc

go 1.13

require (
	github.com/urfave/cli v1.20.0
	gitlab.com/rafaelsq/encstream v0.0.0-20171201112247-79d70677768f
	golang.org/x/crypto v0.0.0-20180219163459-432090b8f568 // indirect
)
